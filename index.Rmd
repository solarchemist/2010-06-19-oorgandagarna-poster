---
layout: post
title: "Conference poster: Oorgandagarna 2010"
date: 2010-06-19T18:00:00+01:00
slug: "oorgandagarna-poster"
tags: ["poster", "Oorgandagarna"]
draft: no
sourcerepo: "https://codeberg.org/solarchemist/2010-06-19-oorgandagarna-poster"
---


```{r packages, echo=FALSE, results='hide', message=FALSE}
library(knitr)
```

```{r global_options, echo=FALSE, results='hide', message=FALSE}
options(digits   = 7,
        width    = 84,
        continue = " ",
        prompt   = "> ",
        warn = 0,
        stringsAsFactors = FALSE)
opts_chunk$set(
   dev        = 'svg',
   out.width  = "100%",
#  fig.width  = 7.50,
# 	fig.height = 5.25,
   fig.align  = 'center',
   echo       = TRUE,
   eval       = TRUE,
   cache      = TRUE,
   collapse   = TRUE,
   results    = 'hide',
   message    = FALSE,
   warning    = FALSE,
   tidy       = FALSE)
```




I attended the conference Oorgandagarna 2010 (["Inorganic Days"](http://www.oorgan.se/)) which was organised by the [Swedish Chemical Society](http://kemisamfundet.se/) at SLU, Uppsala, June 16--19 2010.

We presented the poster "Semiconducting oxides for photoelectrochemical water splitting". I made the poster using LaTeX and TikZ with the `beamerposter` package.


![](assets/100619-poster.png)


<a href="https://dx.doi.org/10.6084/m9.figshare.3569154.v1">
<div style="border: 2px solid blue; padding-left: 6px; padding-top: 2px; background-color: #EBEBEB; color: black; text-align: center;">
The poster and its source files are posted on Figshare.
<br>
https://dx.doi.org/10.6084/m9.figshare.3569154.v1
</div>
</a>




## Tools

- [beamerposter Github page](https://github.com/deselaers/latex-beamerposter)
- [beamerposter on CTAN](http://www.ctan.org/pkg/beamerposter)
- [beamerposter Google Group](https://groups.google.com/forum/#!forum/beamerposter)








## Session info

```{r echo=FALSE, results='markup', cache=F}
sessionInfo()
```
